<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'product'], function () use ($router) {
        Route::get('/', [\App\Http\Controllers\ProductController::class, 'index'])->name('index');
        Route::get('/{product}', [\App\Http\Controllers\ProductController::class, 'show'])->name('show');
        Route::post('/', [\App\Http\Controllers\ProductController::class, 'store'])->name('store');
//        $router->patch('/{product}', ['uses' => 'ProductController@update']);
//        $router->delete('/{product}', ['uses' => 'ProductController@destroy']);
    });
});
