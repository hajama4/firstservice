<?php


namespace Database\Factories;




use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    public function definition()
    {
        return [
            'name' => $this->faker->userName,
            'price' => $this->faker->numberBetween(10,200),
        ];
    }
}