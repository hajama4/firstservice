<?php

namespace App\Http\Repositories;

use App\Models\Product;

class ProductRepository
{
    public function getAll(): ?array
    {
        return Product::query()->get()->toArray();
    }

    public function getOne($product): ?array
    {
        return Product::query()->find($product)->toArray();
    }

    public function setProduct($request)
    {
        $product = new Product();
        $product->fill($request->toArray());
        $saved = $product->save();

        if ($saved) {
            return 'true';
        }

        return 'false';
    }

    public function updateProduct($request, $id)
    {
        $updated = Product::query()->find($id)->update($request->toArray());

        if ($updated) {
            return 'true';
        }

        return 'false';
    }

    public function deleteProduct($product)
    {
        $deleted = Product::query()->find($product)->delete();

        if ($deleted) {
            return 'true';
        }

        return 'false';
    }
}