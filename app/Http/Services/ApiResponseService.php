<?php


namespace App\Http\Services;

class ApiResponseService
{
    public function successResponse($data, $statusCode = '200')
    {
        return response()->json(['data' => $data], $statusCode);
    }

    public function errorResponse($errorMessage, $statusCode)
    {
        return response()->json(['error' => $errorMessage, 'error_code' => $statusCode], $statusCode);
    }
}