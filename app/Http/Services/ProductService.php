<?php


namespace App\Http\Services;

use App\Http\Repositories\ProductRepository;

class ProductService
{
    protected ProductRepository $productRepository;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    public function getProducts(): ?array
    {
        return $this->productRepository->getAll();
    }

    public function getProduct($product): ?array
    {
        return $this->productRepository->getOne($product);
    }

    public function setProduct($request): string
    {
        return $this->productRepository->setProduct($request);
    }

    public function updateProduct($request, $product)
    {
        return $this->productRepository->updateProduct($request, $product);
    }

    public function deleteProduct($product)
    {
        return $this->productRepository->deleteProduct($product);
    }
}