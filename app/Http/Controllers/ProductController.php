<?php

namespace App\Http\Controllers;

use App\Http\Services\ApiResponseService;
use App\Http\Services\ProductService;
use Illuminate\Http\JsonResponse;
use \Illuminate\Http\Request;

class ProductController extends Controller
{
    protected ApiResponseService $apiResponseService;
    protected ProductService $productService;

    public function __construct(
        ApiResponseService $apiResponseService,
        ProductService $productService
    ) {
        $this->apiResponseService = $apiResponseService;
        $this->productService = $productService;
    }

    public function index(): JsonResponse
    {
        $products = $this->productService->getProducts();

        return $this->apiResponseService->successResponse($products, '200');
    }

    public function show($id): JsonResponse
    {
        $product = $this->productService->getProduct($id);

        return $this->apiResponseService->successResponse($product, '200');
    }

    public function store(Request $request): JsonResponse
    {
        $response = $this->productService->setProduct($request);

        return $this->apiResponseService->successResponse($response, '200');
    }

    public function update(Request $request, $product): JsonResponse
    {
        $response = $this->productService->updateProduct($request, $product);

        return $this->apiResponseService->successResponse($response, '200');
    }

    public function destroy($product): JsonResponse
    {
        $response = $this->productService->deleteProduct($product);

        return $this->apiResponseService->successResponse($response, '200');
    }
}